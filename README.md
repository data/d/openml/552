# OpenML dataset: detroit

https://www.openml.org/d/552

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: J.C. Fisher  
**Source**: [StatLib](http://lib.stat.cmu.edu/datasets/) - 1992  
**Please cite**:   

Data on the homicide rate in Detroit for the years 1961-1973. This is the data set called DETROIT in the book 'Subset selection in regression' by Alan J. Miller published in the Chapman & Hall series of monographs on Statistics & Applied Probability, no. 40. The data are unusual in that a subset of three predictors can be found which gives a very much better fit to the data than the subsets found from the Efroymson stepwise algorithm, or from forward selection or backward elimination.

The original data were given in appendix A of `Regression analysis and its application: A data-oriented approach' by Gunst & Mason, Statistics textbooks and monographs no. 24, Marcel Dekker. It has caused problems because some copies of the Gunst & Mason book do not contain all of the data, and because Miller does not say which variables he used as predictors and which is the dependent variable. (HOM was the dependent variable, and the predictors were FTP ... WE)

The data were collected by J.C. Fisher and used in his paper: "Homicide in Detroit: The Role of Firearms", Criminology, vol.14, 387-400 (1976)

Attributes:  
>
FTP    - Full-time police per 100,000 population  
UEMP   - % unemployed in the population  
MAN    - number of manufacturing workers in thousands  
LIC    - Number of handgun licences per 100,000 population  
GR     - Number of handgun registrations per 100,000 population  
CLEAR  - % homicides cleared by arrests  
WM     - Number of white males in the population  
NMAN   - Number of non-manufacturing workers in thousands  
GOV    - Number of government workers in thousands  
HE     - Average hourly earnings  
WE     - Average weekly earnings  
HOM    - Number of homicides per 100,000 of population  
ACC    - Death rate in accidents per 100,000 population  
ASR    - Number of assaults per 100,000 population

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/552) of an [OpenML dataset](https://www.openml.org/d/552). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/552/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/552/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/552/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

